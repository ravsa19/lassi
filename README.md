ALL CONTENTS HEREIN (THE DICTIONARY LEARNING AND LASSI IMAGE RECONSTRUCTION SOFTWARE) ARE MADE AVAILABLE FOR YOUR OWN PERSONAL USE, AND STRICTLY FOR RESEARCH/ACADEMIC AND NON-COMMERICAL USE ONLY.

------------------------------------------------------------------------------------------------------------------------------------------

PLEASE ACKNOWLEDGE AND CITE THE PAPERS LISTED BELOW AND, IN THE CODE, WHENEVER YOU REPORT ON WORK USING THE SOFTWARE PACKAGE IN THIS DIRECTORY, OR ANY FUTURE VERSION CREATED FROM IT.

------------------------------------------------------------------------------------------------------------------------------------------

This release of this software is subject to copyright by the authors below.

The folder contains the MATLAB implementation of algorithms described in the following papers:

[1] S. Ravishankar, B. E. Moore, R. R. Nadakuditi, and J. A. Fessler, &quot;LASSI: A Low-rank and Adaptive Sparse Signal Model for Highly Accelerated Dynamic Imaging,&quot; in IEEE Image, Video, and Multidimensional Signal Processing (IVMSP) Workshop, 2016, pp. 1-5. [[https://ieeexplore.ieee.org/document/7812765](https://ieeexplore.ieee.org/document/7812765)]

[2] S. Ravishankar, B. E. Moore, R. R. Nadakuditi, and J. A. Fessler, &quot;Low-rank and Adaptive Sparse Signal (LASSI) Models for Highly Accelerated Dynamic Imaging,&quot; in IEEE Transactions on Medical Imaging, vol. 36, no. 5, pp. 1116-1128, May 2017.  [[https://ieeexplore.ieee.org/document/7528216](https://ieeexplore.ieee.org/document/7528216)]

[3] S. Ravishankar, R. R. Nadakuditi, and J. A. Fessler, &quot;Efficient Sum of Outer Products Dictionary Learning (SOUP-DIL) and Its Application to Inverse Problems,&quot; in IEEE Transactions on Computational Imaging, vol. 3, no. 4, pp. 694-709, Dec. 2017.  [[https://ieeexplore.ieee.org/document/7907280](https://ieeexplore.ieee.org/document/7907280)]

[4] S. Ravishankar, B. E. Moore, R. R. Nadakuditi, and J. A. Fessler, &quot;Efficient Learning of Dictionaries with Low-rank Atoms,&quot; in IEEE Global Conference on Signal and Information Processing (GlobalSIP), 2016, pp. 222-226.  [[https://ieeexplore.ieee.org/document/7905836](https://ieeexplore.ieee.org/document/7905836)]

This work was supported in part by the following grants: ONR grant N00014-15-1-2141, DARPA Young Faculty Award D14AP00086, ARO MURI grants W911NF-11-1-0391 and 2015-05174-05, NIH grants R01 EB023618 and P01 CA059827, and a UM-SJTU seed grant.

------------------------------------------------------------------------------------------------------------------------------------------

The following is a brief description of the MATLAB files, which can be used to simulate the experimental scenarios in our papers. Please read the specific MATLAB files for detailed information about them.

**MAIN AND SIMULATION CODES** :

1.  LASSI.m - MATLAB function for dynamic magnetic resonance image reconstruction from k-t space measurements using a low-rank + adaptive (learned) dictionary sparse decomposition.

2.  soupDil.m - MATLAB function that learns a synthesis dictionary from signals or image patches with a rank constraint (upper bound) on reshaped dictionary columns by using a block coordinate descent algorithm (see papers [3] and [4] above).

3.  unitaryDil.m – MATLAB function that learns a unitary dictionary from data by using an alternating minimization algorithm.

4.  OptShrink.m – MATLAB function that applies the OptShrink algorithm to its input to generate a rank-r output.

5.  patchRPCA.m – MATLAB function implementing the image reconstruction step of the LASSI algorithm. The low-rank and adaptive dictionary sparse components of the dynamic image sequence are estimated.

6.  patchInds.m – MATLAB function that generates an index matrix for fast multidimensional patch extraction.

------------------------------------------------------------------------------------------------------------------------------------------

**PARAMETER SETTINGS:**

The MATLAB functions above have various input parameters that need to be carefully/optimally set at the time of use. Some examples of parameter values (for specific scenarios) can be found in our papers and in our codes but note that these may not necessarily provide the best or even acceptable performance for your data.

------------------------------------------------------------------------------------------------------------------------------------------

If you have any questions on the software package, you are welcome to contact the authors.